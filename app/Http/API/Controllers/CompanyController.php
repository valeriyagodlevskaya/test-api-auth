<?php


namespace App\Http\API\Controllers;


use App\Http\Controllers\Controller;
use App\Http\API\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {
        return response()->json(Company::paginate(15), 200);
    }

}
